import Vue from 'vue'
import VueRouter from 'vue-router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import App from './App'

import Caixa from './components/Caixa'
import Cliente from './components/Cliente'
import Fiado from './components/Fiado'
import Gerencia from './components/Gerencia'
import Produtos from './components/Produtos'
import Relatorios from './components/Relatorios'

Vue.config.productionTip = false

Vue.use(VueMaterial)
Vue.use(VueRouter)

const routes = [
  {path: '/caixa', component: Caixa},
  {path: '/clientes', component: Cliente},
  {path: '/fiado', component: Fiado},
  {path: '/gerencia', component: Gerencia},
  {path: '/produtos', component: Produtos},
  {path: '/relatorios', component: Relatorios}]

const router = new VueRouter({
  routes // short for `routes: routes`
})

/* eslint-disable eol-last */
/* eslint-disable no-new */
/* eslint-disable */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')